/**
 * @format
 */

import {AppRegistry, View} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Component } from 'react';

AppRegistry.registerComponent(appName, () => App);

